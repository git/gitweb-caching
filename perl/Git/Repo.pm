=head1 NAME

Git::Repo - Read-only access to the Git repositories.

=head1 DESCRIPTION

Git::Repo aims to provide low-level access to Git repositories.  For
instance, you can resolve object names (like 'HEAD~2') to SHA1s, and
inspect objects.  It does not attempt to be a wrapper around the git
plumbing or porcelain commands.

Error handling is simple: On a consistent repository, the Perl
interface will never die.  You can use the get_sha1 method to resolve
arbitrary object names or check the existence of SHA1 hashes; get_sha1
will return undef if the object does not exist in the repository.  Any
SHA1 that is returned by get_sha1 can be safely passed to the other
Git::Repo methods.

=head1 SYNOPSIS

  use Git::Repo;

  my $repo = Git::Repo->new(
      repo_dir => '/path/to/repository.git',
      git_binary => '/usr/bin/git');
  my $sha1 = $repo->get_sha1('HEAD');
  print "Last log message:\n\n" . $repo->get_commit($sha1)->message;

=cut


use strict;
use warnings;
# We could be compatible to Perl 5.6, but it doesn't provide sane pipe
# handling (sane meaning does not go through shell, and allows for
# accessing the exit code), so we require 5.8 until someone decides to
# implement fork/exec-based pipe handling, plus possibly workarounds
# for Windows brokenness.
use 5.008;


package Git::Repo;

use Git::Tag;
use Git::Commit;

use IPC::Open2 qw(open2);
use IO::Handle;

use base qw(Exporter);

our @EXPORT = qw();
our @EXPORT_OK = qw();

# Auxiliary subroutines

sub _assert_opts {
	die "must have an even number of arguments for named options"
	    unless $#_ % 2;
}

sub _assert_sha1 {
	my $sha1 = shift;
	die "'$sha1' is not a SHA1 (need to use get_sha1?)"
	    unless $sha1 && $sha1 =~ /^[a-f0-9]{40}$/;
}


=head1 METHODS

=head2 General methods

=over

=item $repo = Git::Repo->new(%opts)

Return a new Git::Repo object.  The following options are supported:

=over

=item 'repo_dir'

The directory of the repository (mandatory).

Note that this option is working-copy agnostic; you need to
instantiate it with the working copy's .git directory as the
'repo_dir' option.

=item 'git_binary'

The name or full path of the git binary (default: 'git').

=back

Calling this method is free, since it does not check whether the
repository exists.  Trying to access the repository through one of the
instance methods will fail if it doesn't exist though.

Examples:

    $repo = Git::Repo->new(repo_dir => '/path/to/repository.git');
    $repo = Git::Repo->new(repo_dir => '/path/to/working_copy/.git');

=cut

sub new {
	my $class = shift;
	_assert_opts @_;
	my $self = {@_};
	bless $self, $class;
	die 'no repo_dir given' unless $self->{repo_dir};
	return $self;
}

=item $repo->repo_dir

Return the directory of the repository (.../.git in case of a working
copy).

=cut

sub repo_dir {
	shift->{repo_dir}
}

=item $repo->git_binary

Return the name of or path to the git binary (used with exec).

=cut

sub git_binary {
	shift->{git_binary}
}

# Return the first items of the git command line, for instance
# qw(/usr/bin/git --git-dir=/path/to/repo.git).
sub _git_cmd {
	my $self = shift;
	return ($self->git_binary || 'git', '--git-dir=' . $self->repo_dir);
}


=back

=head2 Inspecting the repository

=over

=item $repo->get_sha1($extended_object_identifier)

Look up the object identified by $extended_object_identifier and
return its SHA1 hash in scalar context or its ($sha1, $type, $size) in
list context, or undef or () if the lookup failed, where $type is one
of 'tag', 'commit', 'tree', or 'blob'.

See L<git-rev-parse(1)>, section "Specifying Revisions", for the
syntax of the $extended_object_identifier string.

Note that even if you pass a SHA1 hash, its existence is still
checked, and this method returns undef or () if it doesn't exist in
the repository.

=cut

sub get_sha1 {
	my ($self, $object_id) = @_;
	die 'no object identifier given' unless $object_id;
	die 'object identifier must not contain newlines' if $object_id =~ /\n/;
	unless ($self->{sha1_stdout}) {
		# Open bidi pipe the first time get_sha1 is called.
		# open2 raises an exception on error, no need to 'or die'.
		open2($self->{sha1_stdout}, $self->{sha1_stdin},
		      $self->_git_cmd, 'cat-file', '--batch-check');
	}
	$self->{sha1_stdin}->print("$object_id\n")
	    or die 'cannot write to pipe';
	my $output = $self->{sha1_stdout}->getline
	    or die 'cannot read from pipe';
	chomp $output;
	return if $output =~ /missing$/;
	my ($sha1, $type, $size) =
	    ($output =~ /^([0-9a-f]{40}) ([a-z]+) ([0-9]+)$/)
	    or die "invalid response: $output";
	return wantarray ? ($sha1, $type, $size) : $sha1;
}

=item $repo->get_object($sha1)

Return the content (as a string) of the object identified by $sha1, or
die if no such object exists in the repository.  In list context,
return the ($type, $content) of the object.

Note that you may want to use the higher-level methods get_commit and
get_tag instead.

=cut

# Possible to-do items: Add optional $file_handle parameter.  Guard
# against getting huge blobs back when we don't expect it (for
# instance, we could limit the size and send SIGPIPE to git if we get
# a blob that is too large).

sub get_object {
	my ($self, $sha1) = @_;
	_assert_sha1($sha1);

	unless ($self->{object_stdout}) {
		# Open bidi pipe the first time get_object is called.
		# open2 raises an exception on error, no need to 'or die'.
		open2($self->{object_stdout}, $self->{object_stdin},
		      $self->_git_cmd, 'cat-file', '--batch');
	}
	$self->{object_stdin}->print("$sha1\n") or die 'cannot write to pipe';
	my ($ret_sha1, $type, $size) =
	    split ' ', $self->{object_stdout}->getline
	    or die 'cannot read from pipe';
	die "'$sha1' not found in repository" if $type eq 'missing';
	$self->{object_stdout}->read(my $content, $size);
	$self->{object_stdout}->getline;  # eat trailing newline
	return wantarray ? ($type, $content) : $content;
}

=item $repo->get_commit($commit_sha1)

Return a new L<Git::Commit> instance referring to the commit object
with SHA1 $commit_sha1.

=cut

sub get_commit {
	my ($self, $sha1) = @_;
	_assert_sha1($sha1);
	return Git::Commit->new($self, $sha1);
}

=item $repo->get_tag($tag_sha1)

Return a new L<Git::Tag> instance referring to the tag object with SHA1
$tag_sha1.

=cut

sub get_tag {
	my ($self, $sha1) = @_;
	_assert_sha1($sha1);
	return Git::Tag->new($self, $sha1);
}

=item $repo->name_rev($committish_sha1, $tags_only = 0)

Return a symbolic name for the commit identified by $committish_sha1,
or undef if no name can be found; see L<git-name-rev(1)> for details.
If $tags_only is true, no branch names are used to name the commit.

=cut

sub name_rev {
	my ($self, $sha1, $tags_only) = @_;
	_assert_sha1($sha1);

	# Note that we cannot use a bidi pipe here since name git
	# name-rev --stdin has an excessively high start-up time.
	# http://thread.gmane.org/gmane.comp.version-control.git/85531
	open my $fh, '-|', $self->_git_cmd, 'name-rev',
		$tags_only ? '--tags' : (), '--name-only', $sha1
	    or die 'error calling git binary';
	chomp(my $name = <$fh>);
	close $fh or die 'git name-rev returned an unexpected error';
	return $name eq 'undefined' ? undef : $name;
}


1;
