=head1 NAME

Git::Commit - Object-oriented interface to Git commit objects.

=head1 DESCRIPTION

Git::Commit is a class representing a commit object in a Git
repository.  It stringifies to the commit object's SHA1.

=cut

use strict;
use warnings;


package Git::Commit;

use base qw(Git::Object);


# Keep documentation in one place to save space.

=head1 METHODS

=head2 General Methods

=over

=item $commit = Git::Commit->new($repo, $sha1)

Return a new Git::Commit instance for a commit object with $sha1 in
repository $repo.

The commit object is loaded lazily.  Hence, calls to this method are
free, and it does not check whether $sha1 exists and has the right
type.  However, accessing any of the commit object's properties will
fail if $sha1 is not a valid commit object.

Note that $sha1 must be the SHA1 of a commit object; tag objects are
not dereferenced.

The author, committer and message methods return Unicode strings,
decoded according to the "encoding" header, with UTF-8 and then
Latin-1 as fallbacks.  (These Unicode strings can contain code points
greater than 256 and are *not* UTF-8 strings; see man perlunitut on
how Perl handles Unicode.)

You will usually want to call $repo->get_commit($sha1) instead of
instantiating this class directly; see L<Git::Repo>.

=item $obj->repo

Return the Git::Repo instance this object was instantiated with.

=item $obj->sha1

Return the SHA1 of this commit object, as provided at instantiation time.

=back

=head2 Property Methods

Calling any of these methods will cause the commit object to be loaded
from the repository, if it hasn't been loaded already.

=over

=item $commit->tree

Return an object that stringifies to the SHA1 of the tree that this
commit object refers to.  (Currently this returns an actual string,
but don't rely on it.)

=item $commit->parents

Return an array of zero or more parent commit objects.  Note that
commit objects stringify to their respective SHA1s, so you can
alternatively treat this as an array of SHA1 strings.

=item $commit->author

Return the author of this commit object as a Unicode string.

=item $commit->committer

Return the committer of this commit object as a Unicode string.

=item $commit->message

Return the commit message of this commit object as a Unicode string.

=item $commit->encoding

Return the encoding header of the commit object, or undef if no
encoding header is present; note that Git::Commit does the necessary
decoding for you, so you should not normally need this method.

=back

=cut


sub tree {
	my $self = shift;
	$self->_load;
	return $self->{tree};
}

sub parents {
	my $self = shift;
	$self->_load;
	return map { ref($self)->new($self->repo, $_) } @{$self->{parents}};
}

sub author {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{author});
}

sub committer {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{committer});
}

sub message {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{message});
}

sub encoding {
	my $self = shift;
	$self->_load;
	return $self->{encoding};
}

# Auxiliary method to load (and parse) the commit object from the
# repository if it hasn't already been loaded.  Optional parameter:
# The raw contents of the commit object; the commit object will be
# retrieved from the repository if that parameter is not given.
sub _load {
	my ($self, $raw_text) = shift;
	return if exists $self->{message};  # already loaded

	my $sha1 = $self->sha1;
	if (!defined $raw_text) {
		# Retrieve from the repository.
		(my $type, $raw_text) = $self->repo->get_object($sha1);
		die "$sha1 is a $type object (expected a commit object)"
		    unless $type eq 'commit';
	}

	(my $header, $self->{message}) = split "\n\n", $raw_text, 2;
	# Parse header.
	for my $line (split "\n", $header) {
		local $/ = "\n"; # for chomp
		chomp($line);
		my ($key, $value) = split ' ', $line, 2;
		if ($key eq 'tree') {
			$self->{tree} = $value;
		} elsif ($key eq 'parent') {
			push @{$self->{parents}}, $value;
		} elsif ($key eq 'author') {
			$self->{author} = $value;
		} elsif ($key eq 'committer') {
			$self->{committer} = $value;
		} elsif ($key eq 'encoding') {
			$self->{encoding} = $value;
		} else {
			# Ignore unrecognized header lines.
		}
	}
	undef;
}


1;
