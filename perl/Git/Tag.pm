=head1 NAME

Git::Tag - Object-oriented interface to Git tag objects.

=head1 DESCRIPTION

Git::Tag is a class representing a tag object in a Git repository.  It
stringifies to the tag object's SHA1.

=cut

use strict;
use warnings;


package Git::Tag;

use base qw(Git::Object);


# Keep documentation in one place to save space.

=head1 METHODS

=head2 General Methods

=over

=item $tag = Git::Tag->new($repo, $sha1)

Return a new Git::Tag instance for a tag object with $sha1 in
repository $repo.

The tag object is loaded lazily.  Hence, calls to this method are
free, and it does not check whether $sha1 exists and has the right
type.  However, accessing any of the tag object's properties will fail
if $sha1 is not a valid tag object.

The tagger and message methods return Unicode strings, decoded
according to the "encoding" header, with UTF-8 and then Latin-1 as
fallbacks.  (These Unicode strings can contain code points greater
than 256 and are *not* UTF-8 strings; see man perlunitut on how Perl
handles Unicode.)

You will usually want to call $repo->get_tag($sha1) instead of
instantiating this class directly; see L<Git::Repo>.

=item $obj->repo

Return the Git::Repo instance this object was instantiated with.

=item $obj->sha1

Return the SHA1 of this tag object, as provided at instantiation time.

=back

=head2 Property Methods

Calling any of these methods will cause the commit object to be loaded
from the repository, if it hasn't been loaded already.

=over

=item $tag->object

Return the SHA1 string of the object referenced by this tag.

=item $tag->type

Return the type of the referenced object, as claimed by the tag
object.  This is usually 'commit', but can be any of 'tag', 'commit',
'tree', or 'blob'.

=item $tag->tagger

Return the tagger string of this tag object.

=item $tag->message

Return the undecoded tag message of this tag object.

=item $tag->encoding

Return the encoding header of the tag object, or undef if no encoding
header is present; note that Git::Tag does the necessary decoding for
you, so you should not normally need this method.

=back

=cut


sub object {
	my $self = shift;
	$self->_load;
	return $self->{object};
}

sub type {
	my $self = shift;
	$self->_load;
	return $self->{type};
}

sub tag {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{tag});
}

sub tagger {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{tagger});
}

sub message {
	my $self = shift;
	$self->_load;
	return $self->_decode($self->{message});
}

sub encoding {
	my $self = shift;
	$self->_load;
	return $self->{encoding};
}

# Auxiliary method to load (and parse) the tag object from the
# repository if it hasn't already been loaded.  Optional parameter:
# The raw contents of the tag object; the tag object will be retrieved
# from the repository if that parameter is not given.
sub _load {
	my ($self, $raw_text) = shift;
	return if exists $self->{message};  # already loaded

	my $sha1 = $self->sha1;
	if (!defined $raw_text) {
		(my $type, $raw_text) = $self->repo->get_object($sha1);
		die "$sha1 is a $type object (expected a tag object)"
		    unless $type eq 'tag';
	}

	(my $header, $self->{message}) = split "\n\n", $raw_text, 2;
	# Parse header.
	for my $line (split "\n", $header) {
		local $/ = "\n"; # for chomp
		chomp($line);
		my ($key, $value) = split ' ', $line, 2;
		if ($key eq 'object') {
			$self->{object} = $value;
		} elsif ($key eq 'type') {
			$self->{type} = $value;
		} elsif ($key eq 'tag') {
			$self->{tag} = $value;
		} elsif ($key eq 'tagger') {
			$self->{tagger} = $value;
		} elsif ($key eq 'encoding') {
			$self->{encoding} = $value;
		} else {
			# Ignore unrecognized header lines.
		}
	}
	undef;
}


1;
