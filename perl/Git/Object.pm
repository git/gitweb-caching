=head1 NAME

Git::Object - Object-oriented interface to Git objects (base class).

=head1 DESCRIPTION

Git::Object is a base class that provides access to commit, tag and
(unimplemented) tree objects.  See L<Git::Commit> and L<Git::Tag>.

Objects are loaded lazily, and hence instantiation is free.
Git::Object instances stringify to their SHA1s.

=cut


use strict;
use warnings;


package Git::Object;

use Encode qw(decode);

use base qw(Exporter);

our @EXPORT = qw();
our @EXPORT_OK = qw();

use overload
    '""' => \&sha1;

=head1 METHODS

=over

=item Git::Object->new($repo, $sha1)

Return a new Git::Object instance for the object with $sha1 in the
repository $repo (a Git::Repo instance).

Note that this method does not check whether the object exists in the
repository.  Trying to accessing its properties through a subclass
will fail if the object doesn't exist, however.

=cut

sub new {
	my ($class, $repo, $sha1) = @_;
	die "$repo is not a Git::Repo instance" unless $repo->isa('Git::Repo');
	my $self = {repo => $repo, sha1 => $sha1};
	return bless $self, $class;
}

=item $obj->repo

Return the Git::Repo instance this object was instantiated with.

=cut

sub repo {
	shift->{repo}
}

=item $obj->sha1

Return the SHA1 of this object.

=cut

sub sha1 {
	shift->{sha1}
}

# Helper method: Decode the given octets into a Unicode string, trying
# the $self->{encoding} encoding first, if defined, then trying UTF-8,
# then falling back to Latin 1.

sub _decode {
	my ($self, $octets) = @_;
	my $string;
	# Try $self->{encoding}:
	eval { $string = decode($self->{encoding}, $octets, Encode::FB_CROAK) }
	    if $self->{encoding};
	# ... else try UTF-8:
	eval { $string = decode('utf-8', $octets, Encode::FB_CROAK) }
	    unless defined $string;
	# ... else fall back to Latin 1 (the first 256 Unicode code
	# points coincide with Latin 1):
	$string = $octets unless defined $string;
	return $string;
}


1;
