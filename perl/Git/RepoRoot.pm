=head1 NAME

Git::RepoRoot - A factory class representing a root directory
containing Git repositories.

=head1 DESCRIPTION

Git::RepoRoot is a factory class to create L<Git::Repo> instances that
are located under a common root directory.  It also allows for
specifying options that all Git::Repo instances will be created with.

Using Git::RepoRoot to create Git::Repo instances is entirely
optional, but can be more convenient than instantiating them directly.

=cut


use strict;
use warnings;


package Git::RepoRoot;

use File::Spec;

use Git::Repo;

use base qw(Exporter);

our @EXPORT = qw();
our @EXPORT_OK = qw();

=head1 METHODS

=over

=item $repo_root = Git::RepoRoot->new(%opts)

Return a new Git::RepoRoot object.  The following options are
supported:

=over

=item 'root_dir'

The directory holding all repositories.

=back

All other options will be passed through to Git::Repo->new.

Example:

    $repo_root = Git::RepoRoot->new(root_dir => '/pub/git',
                                    git_binary => '/usr/bin/git');

=cut

sub new {
	my $class = shift;
	Git::Repo::_assert_opts(@_);
	my $self = {@_};
	bless $self, $class;
	die 'no root_dir given' unless $self->{root_dir};
	return $self;
}

=item $repo_root->repo(%opts)

Return a new L<Git::Repo> object.  The following options are
supported:

=over

=item 'root_dir'

The path of the repository relative to the repository root.

=item 'repo_class'

The Repo class to instantiate (default: 'Git::Repo').

=back

All other options are passed through to Git::Repo.

=cut

sub repo {
	my $self = shift;
	Git::Repo::_assert_opts(@_);
	my %opts = (%$self, @_);
	die 'no repo_dir given' unless $opts{repo_dir};
	# not technically necessary, but to guard against errors in the caller:
	die "you passed an absolute path ($opts{repo_dir})"
	    if $opts{repo_dir} =~ m!^/!;
	my $repo_class = delete $opts{repo_class} || 'Git::Repo';
	$opts{repo_dir} = File::Spec->catfile($self->{root_dir}, $opts{repo_dir});
	return $repo_class->new(%opts);
}


1;
