#!/bin/sh
#
# Copyright (c) 2008 Jakub Narebski
# Copyright (c) 2008 Lea Wiemann
#

# This test supports the --long-tests option.

# This test only runs on Perl 5.8 and later versions, since both
# Gitweb and Test::WWW::Mechanize::CGI require Perl 5.8.

test_description='gitweb tests (using WWW::Mechanize)

This test uses Test::WWW::Mechanize::CGI to test gitweb.'

# helper functions

safe_chmod () {
	chmod "$1" "$2" &&
	if [ "$(git config --get core.filemode)" = false ]
	then
		git update-index --chmod="$1" "$2"
	fi
}

. ./test-lib.sh

# check if test can be run
"$PERL_PATH" -e 'use 5.008' >/dev/null 2>&1 || {
	test_expect_success \
		'skipping gitweb tests, Perl 5.8 or newer required' :
	test_done
	exit
}

"$PERL_PATH" -MTest::WWW::Mechanize::CGI -e '' >/dev/null 2>&1 || {
	test_expect_success \
		'skipping gitweb tests, Test::WWW::Mechanize::CGI not found' :
	test_done
	exit
}

# set up test repository
test_expect_success 'set up test repository' '

	echo "Not an empty file." > file &&
	git add file &&
	test_tick && git commit -a -m "Initial commit." &&
	git branch b &&

	echo "New file" > new_file &&
	git add new_file &&
	test_tick && git commit -a -m "File added." &&

	safe_chmod +x new_file &&
	test_tick && git commit -a -m "Mode changed." &&

	git mv new_file renamed_file &&
	test_tick && git commit -a -m "File renamed." &&

	rm renamed_file &&
	ln -s file renamed_file &&
	test_tick && git commit -a -m "File to symlink." &&
	git tag with-symlink &&

	git rm renamed_file &&
	rm -f renamed_file &&
	test_tick && git commit -a -m "File removed." &&

	cp file file2 &&
	git add file2 &&
	test_tick && git commit -a -m "File copied." &&

	echo "New line" >> file2 &&
	safe_chmod +x file2 &&
	test_tick && git commit -a -m "Mode change and modification." &&

	mkdir dir1 &&
	echo "New file with a \"pickaxe test string\"" > dir1/file1 &&
	git add dir1/file1 &&
	test_tick && git commit -a -m "File added in subdirectory." &&
	git tag -m "creating a tag object" tag-object

	git checkout b &&
	echo "Branch" >> b &&
	git add b &&
	test_tick && git commit -a -m "On branch" &&
	git checkout master &&
	test_tick && git merge b
'

# set up empty repository
# create this as a subdirectory of trash directory; not pretty, but simple
test_expect_success 'set up empty repository' '

	mkdir empty.git &&
	cd empty.git &&
	git init --bare &&
	cd ..
'

# set up gitweb configuration
safe_pwd="$("$PERL_PATH" -MPOSIX=getcwd -e 'print quotemeta(getcwd)')"
large_cache_root="$TEST_DIRECTORY/t9503/large_cache.tmp"
test_expect_success 'create file cache directory' \
	'mkdir -p "$large_cache_root"'
cat >gitweb_config.perl <<EOF
# gitweb configuration for tests

our \$version = "current";
our \$GIT = "$GIT_EXEC_PATH/git";
our \$projectroot = "$safe_pwd";
our \$project_maxdepth = 8;
our \$home_link_str = "projects";
our \$site_name = "[localhost]";
our \$site_header = "";
our \$site_footer = "";
our \$home_text = "indextext.html";
our @stylesheets = ("file:///stylesheet");
our \$logo = "file:///logo";
our \$favicon = "file:///favicon";
our \$projects_list = "";
our \$export_ok = "";
our \$strict_export = "";
our %feature;
\$feature{'blame'}{'default'} = [1];

our \$large_cache_root = "$large_cache_root";
if (eval { require Cache::MemoryCache; 1 }) {
	our \$cache = Cache::MemoryCache->new;
}


1;
__END__
EOF

cat >.git/description <<EOF
t9503-gitweb-Mechanize test repository
EOF

GITWEB_CONFIG="$(pwd)/gitweb_config.perl"
export GITWEB_CONFIG

# run tests

test_external \
	'test gitweb output' \
	"$PERL_PATH" "$TEST_DIRECTORY/t9503/test.pl"

test_expect_success 'remove file cache directory' \
	'rm -rf "$large_cache_root"'

test_done
