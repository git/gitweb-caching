#!/usr/bin/perl
use lib (split(/:/, $ENV{GITPERLLIB}));

use warnings;
use strict;

use Test::More qw(no_plan);
use Cwd;
use File::Basename;
use File::Temp;

BEGIN { use_ok('Git::Repo') }

sub dies_ok (&;$) {
	my ($coderef, $descr) = @_;
	eval { $coderef->(); };
	ok($@, $descr);
}

sub lives_ok (&;$) {
	my ($coderef, $descr) = @_;
	eval { $coderef->(); };
	ok(!$@, $descr);
}

our $old_stderr;
sub discard_stderr {
	open our $old_stderr, ">&", STDERR or die "cannot save STDERR";
	close STDERR;
}
sub restore_stderr {
	open STDERR, ">&", $old_stderr or die "cannot restore STDERR";
}

# set up
our $abs_wc_dir = Cwd->cwd;
ok(our $r = Git::Repo->new(repo_dir => "./.git"), 'open repository');
sub rev_parse {
	my $name = shift;
	chomp(my $sha1 = `git rev-parse $name 2> /dev/null`);
	$sha1 or undef;
}

my @revisions = split /\s/, `git-rev-list --first-parent HEAD`;
my $head = $revisions[0];

# get_sha1
is($r->get_sha1('HEAD'), $head, 'get_sha1: scalar');
is($r->get_sha1('HEAD'), $head, 'get_sha1: scalar, repeated');
my($sha1, $type, $head_size) = $r->get_sha1('HEAD');
is($sha1, $head, 'get_sha1: array (SHA1)');
is($type, 'commit', 'get_sha1: array (commit)');
ok($head_size > 0, 'get_sha1: array (size)');

# get_object
is_deeply([$r->get_object($r->get_sha1("$revisions[-1]:file1"))], ['blob', "test file 1\n"], 'get_object: blob');
is_deeply([$r->get_object($r->get_sha1("$revisions[-1]:file1"))], ['blob', "test file 1\n"], 'get_object: blob, repeated');
dies_ok { $r->get_object('0' x 40) } 'get_object: non-existent sha1';

# get_commit
isa_ok($r->get_commit($revisions[-1]), 'Git::Commit',
       'get_commit: returns Git::Commit object');

# get_tag
isa_ok($r->get_tag($r->get_sha1('tag-object-1')), 'Git::Tag',
       'get_tag: returns Git::Tag object');

# name_rev
is($r->name_rev($revisions[-2]), 'branch-2', 'name_rev: branch');
is($r->name_rev($head, 1), undef, 'name_rev: branch, tags only');
is($r->name_rev($revisions[-1]), 'tags/tag-object-1^0', 'name_rev: tag object');
is($r->name_rev($revisions[-1], 1), 'tag-object-1^0', 'name_rev: tag object, tags only');



# Git::Commmit
print "# Git::Commit:\n";

BEGIN { use_ok('Git::Commit') }

my $invalid_commit = Git::Commit->new($r, '0' x 40);
is($invalid_commit->sha1, '0' x 40, 'new, sha1: accept invalid SHA1');
dies_ok { $invalid_commit->tree } 'die on accessing properties of invalid SHA1s';

$invalid_commit = Git::Commit->new($r, $r->get_sha1('HEAD:')); # tree, not commit
dies_ok { $invalid_commit->tree } 'die on accessing properties of non-commit objects';

my $c = Git::Commit->new($r, $revisions[-2]);
is($c->repo, $r, 'repo: basic');
is($c->sha1, $revisions[-2], 'sha1: basic');
is($c->{parents}, undef, 'lazy loading: not loaded after reading SHA1');
is($c->tree, $r->get_sha1("$revisions[-2]:"), 'tree: basic');
ok($c->{parents}, 'lazy loading: loaded after reading tree');
is_deeply([$c->parents], [$revisions[-1]], 'parents: basic');
like($c->author, qr/A U Thor <author\@example.com> [0-9]+ \+0000/, 'author: basic');
like($c->committer, qr/C O Mitter <committer\@example.com> [0-9]+ \+0000/, 'committer: basic');
is($c->encoding, undef, 'encoding: undef');
is($c->message, "second commit\n", 'message: basic');
is($c, $c->sha1, 'stringify: basic');

# error handling
dies_ok { Git::Commit->new($r, $r->get_sha1('tag-object-3'))->_load }
    'new: pass tag SHA1 (dies)';
dies_ok { Git::Commit->new($r, '0' x 40)->_load }
    'new: pass invalid SHA1 (dies)';


# Git::Tag
print "# Git::Tag:\n";

BEGIN { use_ok('Git::Tag') }

# We don't test functionality inherited from Git::Object that we
# already tested in the Git::Commit tests.

my $t = Git::Tag->new($r, $r->get_sha1('tag-object-1'));
is($t->tag, 'tag-object-1', 'tag: basic');
is($t->object, $revisions[-1], 'object: basic');
is($t->type, 'commit', 'tag: type');
like($t->tagger, qr/C O Mitter <committer\@example.com> [0-9]+ \+0000/, 'tagger: basic');
is($t->encoding, undef, 'encoding: undef');
is($t->message, "tag message 1\n", 'message: basic');

# error handling
dies_ok { Git::Tag->new($r, $head)->_load } 'new: pass commit SHA1 (dies)';
dies_ok { Git::Tag->new($r, '0' x 40)->_load } 'new: pass invalid SHA1 (dies)';


# Git::RepoRoot
print "# Git::RepoRoot:\n";

BEGIN { use_ok('Git::RepoRoot'); }

my $reporoot = Git::RepoRoot->new(root_dir => $abs_wc_dir);
is($reporoot->repo(repo_dir => '.git')->get_sha1('HEAD'), $head,
   'repo: basic');
